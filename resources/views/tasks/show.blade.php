@extends('layouts.app')

@section('content')
<div class="d-grid gap-1 d-md-flex justify-content-md-end">
<a href="/tasks" class="btn btn-primary" type="button">Go Back</a>
 @if(!Auth::guest())
        @if(Auth::user()->id ===$post->user_id)
        <a class="btn btn-primary" type="button" href="/tasks/{{$task->id}}/edit">Edit</a>
        <form method="post" action="/tasks/{{ $task->id }}">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">Delete</button>
        </form>
    @endif
</div>
	<h1>
 		{{ $post->title }}
    </h1>

    <p>
        {{ $post->body }}
    </p>
    
@endsection